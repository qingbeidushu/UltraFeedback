<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div align="center" dir="auto">
<a target="_blank" rel="noopener noreferrer" href="/OpenBMB/UltraFeedback/blob/main/figures/logo.png"><img src="/OpenBMB/UltraFeedback/raw/main/figures/logo.png" width="400px" style="max-width: 100%;"></a>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大规模、细粒度、多样化的偏好数据集</font></font></strong></p>
<p align="center" dir="auto">
 <a href="#introduction"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">简介</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">•
 </font></font><a href="#dataset-construction"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据集构建</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">•
 </font></font><a href="#dataset-example"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">示例</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">•
  </font></font><a href="#ultrarm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UltraRM</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> •
  </font></font><a href="#ultrarm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UltraCM</font></font></a>
</p>
</div>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">消息</font></font></h1><a id="user-content-news" class="anchor" aria-label="永久链接：新闻" href="#news"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[2023/12/29]：我们已经修复了</font><a href="https://github.com/OpenBMB/UltraFeedback/issues/8" data-hovercard-type="issue" data-hovercard-url="/OpenBMB/UltraFeedback/issues/8/hovercard"><font style="vertical-align: inherit;">本期</font></a></font><code>overall_score</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中指出的问题，并更新了</font><a href="https://huggingface.co/datasets/openbmb/UltraFeedback" rel="nofollow"><font style="vertical-align: inherit;">HuggingFace</font></a><font style="vertical-align: inherit;">上的数据集</font><font style="vertical-align: inherit;">。详情请参阅下面的“更新”部分。</font></font><a href="https://github.com/OpenBMB/UltraFeedback/issues/8" data-hovercard-type="issue" data-hovercard-url="/OpenBMB/UltraFeedback/issues/8/hovercard"><font style="vertical-align: inherit;"></font></a><font style="vertical-align: inherit;"></font><a href="https://huggingface.co/datasets/openbmb/UltraFeedback" rel="nofollow"><font style="vertical-align: inherit;"></font></a><font style="vertical-align: inherit;"></font></li>
<li><font style="vertical-align: inherit;"></font><a href="https://huggingface.co/openbmb/UltraLM-13b-v2.0" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[2023/09/26]：UltraRM 释放UltraLM-13B-v2.0</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://huggingface.co/openbmb/UltraLM-13b" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UltraLM-13B</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的力量</font><font style="vertical-align: inherit;">！简单的 16 中最佳采样在</font><a href="https://tatsu-lab.github.io/alpaca_eval/" rel="nofollow"><font style="vertical-align: inherit;">AlpacaEval</font></a><font style="vertical-align: inherit;">基准</font><font style="vertical-align: inherit;">上与 text-davinci-003 相比，获胜率达到</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">92.30%</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（UltraLM2，🥇 在 13B 结果中）和</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">91.54% （UltraLM，🥇 在 LLaMA-1 结果中）！</font></font></strong><font style="vertical-align: inherit;"></font><a href="https://tatsu-lab.github.io/alpaca_eval/" rel="nofollow"><font style="vertical-align: inherit;"></font></a><font style="vertical-align: inherit;"></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[2023/09/26]：我们发布了</font></font><a href="https://huggingface.co/datasets/openbmb/UltraFeedback" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UltraFeedback</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据集，以及 UltraFeedback 支持的奖励模型</font></font><a href="https://huggingface.co/openbmb/UltraRM-13b" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UltraRM</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和批评模型</font></font><a href="https://huggingface.co/openbmb/UltraCM-13b" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UltraCM</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">！两者都</font><font style="vertical-align: inherit;">在开源模型上构建了</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新的 SOTA ！</font></font></strong><font style="vertical-align: inherit;"></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更新</font></font></h1><a id="user-content-update" class="anchor" aria-label="永久链接：更新" href="#update"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UltraFeedback 的初始版本包括 2628 个完成项，总分为</font></font><code>10</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">.然而，正如问题</font></font><a href="https://github.com/OpenBMB/UltraFeedback/issues/8" data-hovercard-type="issue" data-hovercard-url="/OpenBMB/UltraFeedback/issues/8/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">#8</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中所指出的，其中许多完成的分数应该为</font></font><code>1</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。直观上，总分为0的完成</font></font><code>10</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">应该是高质量的，这可以反映在其相应的</font></font><code>averaged</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">细粒度分数上。因此，为了纠正分数，我们根据细粒度分数处理了所有潜在错误的完成情况。具体来说，</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具有细粒度分数的完成</font></font><code>&lt;= 2</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可能质量较低，因此</font></font><code>overall_score</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">已将其手动调整为</font></font><code>1</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">另一方面，具有细粒度分数的完成</font></font><code>&gt; 4</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">被认为准确地代表了分数</font></font><code>10</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，因此它们的overall_score保持不变。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于其余的补全，我们在原来的批评的基础上进行了</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">重新注释</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">过程，并对提示进行了轻微的修改。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"></font><code>./src/fix_overall_score_issue.py</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实施细节</font><font style="vertical-align: inherit;">请参阅。</font></font></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">链接</font></font></h1><a id="user-content-links" class="anchor" aria-label="永久链接： 链接" href="#links"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">📜</font></font><a href="https://arxiv.org/abs/2310.01377" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">纸</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🤗</font></font><a href="https://huggingface.co/datasets/openbmb/UltraFeedback" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超反馈</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🤗</font></font><a href="https://huggingface.co/openbmb/UltraRM-13b" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超级RM</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">🤗</font></font><a href="https://huggingface.co/openbmb/UltraCM-13b" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超CM</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">介绍</font></font></h1><a id="user-content-introduction" class="anchor" aria-label="永久链接：简介" href="#introduction"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UltraFeedback是一个</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大规模、细粒度、多样化的偏好数据集</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，用于训练强大的奖励模型和批评模型。我们从不同的资源（包括 UltraChat、ShareGPT、Evol-Instruct、TruthfulQA、FalseQA 和 FLAN，请参阅</font></font><a href="#instruction-sampling"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">了解数据集统计信息）收集了大约 64k 提示。然后，我们使用这些提示来查询多个 LLM（请参阅</font></font><a href="#model-sampling"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的模型列表），并为每个提示生成 4 个不同的响应，总共产生 256k 个样本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为了收集高质量的偏好和文本反馈，我们设计了细粒度的注释指令，其中包含4个不同的方面，即</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令遵循</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">真实性</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">诚实性</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">帮助性</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。然后我们要求 GPT-4 根据说明对收集的样本进行注释。</font></font></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></h1><a id="user-content-features" class="anchor" aria-label="永久链接：特点" href="#features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">规模</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：UltraFeedback 包含 64k 提示、256k 响应和高质量反馈。 RLHF 研究人员可以进一步构建大约 34 万个比较对来训练他们的奖励模型。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多样性</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：作为偏好数据集，多样性是UltraFeedback的核心要求。我们从各种来源收集提示，并查询各种最先进的开源和享有盛誉的模型。为了进一步增加多样性，我们打算选择不同的基础模型，即LLaMA、Falcon、StarChat、MPT、GPT和Bard。我们还应用各种原理来刺激模型以不同的方式完成指令。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高密度</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：UltraFeedback 提供数字和文本反馈。此外，我们编写了细粒度的注释文档来帮助对各个维度的响应进行评分</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据集构建</font></font></h1><a id="user-content-dataset-construction" class="anchor" aria-label="永久链接：数据集构建" href="#dataset-construction"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/OpenBMB/UltraFeedback/blob/main/figures/ultraf.png"><img src="/OpenBMB/UltraFeedback/raw/main/figures/ultraf.png" width="800px" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">指令采样</font></font></h2><a id="user-content-instruction-sampling" class="anchor" aria-label="永久链接：指令采样" href="#instruction-sampling"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们从 6 个公开可用的高质量数据集中抽取了 63,967 条指令。我们包括来自 TruthfulQA 和 FalseQA 的所有指令，从 Evol-Instruct 中随机采样 10k 指令，从 UltraChat 中随机采样 10k，从 ShareGPT 中随机采样 20k。对于FLAN，我们采用分层采样策略，从“CoT”子集中随机采样3k指令，而对其他三个子集每个任务采样10条指令，不包括那些指令过长的子集。</font></font></p>
<div class="highlight highlight-source-json notranslate position-relative overflow-auto" dir="auto"><pre>{
    <span class="pl-ent">"evol_instruct"</span>: <span class="pl-c1">10000</span>, 
    <span class="pl-ent">"false_qa"</span>: <span class="pl-c1">2339</span>,
    <span class="pl-ent">"flan"</span>: <span class="pl-c1">20939</span>, 
    <span class="pl-ent">"sharegpt"</span>: <span class="pl-c1">19949</span>, 
    <span class="pl-ent">"truthful_qa"</span>: <span class="pl-c1">811</span>,
    <span class="pl-ent">"ultrachat"</span>: <span class="pl-c1">9929</span> 
}</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="{
    &quot;evol_instruct&quot;: 10000, 
    &quot;false_qa&quot;: 2339,
    &quot;flan&quot;: 20939, 
    &quot;sharegpt&quot;: 19949, 
    &quot;truthful_qa&quot;: 811,
    &quot;ultrachat&quot;: 9929 
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型抽样</font></font></h2><a id="user-content-model-sampling" class="anchor" aria-label="永久链接：模型采样" href="#model-sampling"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为了防止奖励模型过度拟合某些文本样式或捕获文本样式和奖励之间的虚假相关性，我们选择具有不同大小、架构和训练数据的各个级别的不同基础模型来完成指令。我们建立了 17 个模型池：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">商业型号：GPT-4、GPT-3.5 Turbo、Bard</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">骆驼家族：
</font></font><ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLaMA-2-7B-聊天、LLaMA-2-13B-聊天、LLaMA-2-70B-聊天</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UltraLM-13B、UltraLM-65B</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WizardLM-7B-v1.2、WizardLM-13B-v1.2、WizardLM-70B-v1.0</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">骆驼毛-33B-v1.3</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">羊驼-7B</font></font></li>
</ol>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">非 LLaMA 系列：
</font></font><ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Falcon-40B-指令</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MPT-30B-聊天</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">StarChat-测试版</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pythia-12B</font></font></li>
</ol>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">原则抽样</font></font></h2><a id="user-content-principle-sampling" class="anchor" aria-label="永久链接：原则抽样" href="#principle-sampling"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">遵循[1]和[2]，我们定义了一组原则来从不同方面明确地调整模型行为。我们制定了 4 项原则：乐于助人、诚实、诚实和言语校准。对于每条指令，我们随机采样 4 个模型来完成指令，并且对于每次完成，我们采样一个原则并将其添加到系统提示中以调整模型行为。考虑到不同的数据集概述了不同的特征，并非所有数据集都适合所有原则。我们提供下表来显示每个数据集的主要分布。</font></font></p>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据集</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">原则</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">进化指导</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">100% 有帮助</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">错误的质量保证</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">100% 真实的质量检查</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">水果馅饼</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">60% 有帮助，20% 真实，20% 口头校准</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分享GPT</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">60% 有帮助，20% 诚实，18% 诚实，2% 口头校准</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">真实的质量保证</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">100%真实</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超聊</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">60% 有帮助，20% 诚实，18% 诚实，2% 口头校准</font></font></td>
</tr>
</tbody>
</table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[1] 原则驱动的语言模型自对准，从头开始，最少的人类监督。孙等人。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[2] Orca：从 GPT-4 的复杂解释痕迹中渐进学习。穆克吉等人。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与之前的偏好数据集的比较</font></font></h2><a id="user-content-comparison-with-previous-preference-datasets" class="anchor" aria-label="永久链接：与之前的偏好数据集的比较" href="#comparison-with-previous-preference-datasets"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/OpenBMB/UltraFeedback/blob/main/figures/dataset.png"><img src="/OpenBMB/UltraFeedback/raw/main/figures/dataset.png" width="800px" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超RM</font></font></h1><a id="user-content-ultrarm" class="anchor" aria-label="永久链接：UltraRM" href="#ultrarm"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们训练并发布了基于UltraFeedback的奖励模型UltraRM，以进一步促进对齐研究。 UltraRM 由 LLaMA2-13B 初始化。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">具体来说，我们训练了两个版本的奖励模型，其中 UltraRM-UF 仅在 UltraFeedback 上进行微调，UltraRM 在 UltraFeedback 和来自三个开源数据集（包括</font></font><a href="https://huggingface.co/datasets/Anthropic/hh-rlhf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Anthropic HH-RLHF</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://huggingface.co/datasets/stanfordnlp/SHP" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">斯坦福 SHP</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://huggingface.co/datasets/openai/summarize_from_feedback" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">总结</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在四个公众偏好测试集上，我们的 UltraRM 比其他开源奖励模型实现了 SOTA。</font></font></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/OpenBMB/UltraFeedback/blob/main/figures/ultrarm.png"><img src="/OpenBMB/UltraFeedback/raw/main/figures/ultrarm.png" width="800px" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超CM</font></font></h1><a id="user-content-ultracm" class="anchor" aria-label="永久链接：UltraCM" href="#ultracm"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们训练并发布了基于 UltraFeedback 的评论模型 UltraCM，用于自动评论生成。与UltraRM类似，UltraCM由LLaMA2-13B初始化。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在九个基准数据集上，UltraCM 优于所有开源基准并接近 ChatGPT。</font></font></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/OpenBMB/UltraFeedback/blob/main/figures/ultracm.png"><img src="/OpenBMB/UltraFeedback/raw/main/figures/ultracm.png" width="800px" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据集格式</font></font></h1><a id="user-content-dataset-format" class="anchor" aria-label="永久链接：数据集格式" href="#dataset-format"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="highlight highlight-source-js notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-kos">{</span>
    <span class="pl-s">"source"</span>: <span class="pl-s">"sharegpt"</span><span class="pl-kos">,</span> <span class="pl-c">// the dataset where the instruction comes from</span>
    <span class="pl-s">"instruction"</span>: <span class="pl-s">"I am going to cairo in June of this year, thinking of 4-5 days, what are the best things I could do? Going with family of 5, granddad, grandmom, sister, mom and me. No days can have too much walking but sister, me and mom like adventure sports"</span><span class="pl-kos">,</span>
    <span class="pl-s">"models"</span>: <span class="pl-kos">[</span> <span class="pl-c">// four models that are randomly sampled from the model pool to complete the instruction</span>
        <span class="pl-s">"falcon-40b-instruct"</span><span class="pl-kos">,</span>
        <span class="pl-s">"gpt-4"</span><span class="pl-kos">,</span>
        <span class="pl-s">"starchat"</span><span class="pl-kos">,</span>
        <span class="pl-s">"wizardlm-7b"</span>
    <span class="pl-kos">]</span><span class="pl-kos">,</span>
    <span class="pl-s">"correct_answers"</span>: <span class="pl-kos">[</span><span class="pl-s">"None"</span><span class="pl-kos">]</span> <span class="pl-c">// reference correct answers, set to None except TruthfulQA and FLAN</span>
    <span class="pl-s">"incorrect_answers"</span>: <span class="pl-kos">[</span><span class="pl-s">"None"</span><span class="pl-kos">]</span> <span class="pl-c">// reference incorrect answers, set to None except TruthfulQA and FLAN</span>
    <span class="pl-s">"completions"</span>: <span class="pl-kos">[</span> <span class="pl-c">// four completions corresponding to the four models</span>
        <span class="pl-kos">{</span> <span class="pl-c">// completion 1</span>
            <span class="pl-s">"model"</span>: <span class="pl-s">"falcon-40b-instruct"</span><span class="pl-kos">,</span>
            <span class="pl-s">"principle"</span>: <span class="pl-s">"helpfulness"</span><span class="pl-kos">,</span> <span class="pl-c">// priciple that used to align model behavior</span>
            <span class="pl-s">"custom_system_prompt"</span>: <span class="pl-s">"As an AI assistant, ensure your response offers the perfect blend of accuracy, positivity, and intrigue. Strive to be educational while keeping the user engaged."</span><span class="pl-kos">,</span> <span class="pl-c">// the system prompt corresponding to the principle, sampled from a pool of GPT-4 generated system prompts</span>
            <span class="pl-s">"response"</span>: <span class="pl-s">"Cairo is a city that has something for everyone. Some of the best things you can do are visiting the Pyramids of Giza and the museums housing the ancient Egyptian artifacts, taking a camel ride in the desert, visiting the medieval neighborhoods such as Islamic Cairo or Khan El-Khalili, trying local food in traditional restaurants like Ful medames, or venturing into one of the city's many souks to shop for authentic goods. For adventure sports, you can go whitewater rafting on the Nile, hot air ballooning over the desert, or diving in the Red Sea. Just be mindful of the scorching heat and ensure everyone wears appropriate clothing and drinks enough water. Above all, have fun and enjoy your trip!"</span><span class="pl-kos">,</span>
            <span class="pl-s">"annotations"</span>: <span class="pl-kos">{</span>
                <span class="pl-s">"instruction_following"</span>: 
                    <span class="pl-kos">{</span>
                        <span class="pl-s">"Rating"</span>: <span class="pl-s">"2"</span><span class="pl-kos">,</span>
                        <span class="pl-s">"Rationale"</span>: <span class="pl-s">"The text only partially addresses the task goal by providing a general list of activities in Cairo without considering the specific needs of the family members, such as limited walking ability and adventure preferences."</span>
                    <span class="pl-kos">}</span><span class="pl-kos">,</span>
                <span class="pl-s">"honesty"</span>: 
                    <span class="pl-kos">{</span>
                        <span class="pl-s">"Rating"</span>: <span class="pl-s">"3"</span><span class="pl-kos">,</span>
                        <span class="pl-s">"Rationale"</span>: <span class="pl-s">"The response is confident but contains subtle mistakes. It suggests whitewater rafting on the Nile and diving in the Red Sea, which are not feasible for a 4-5 day trip to Cairo. The rest of the suggestions are accurate and relevant to the family's preferences."</span>
                    <span class="pl-kos">}</span><span class="pl-kos">,</span>
                <span class="pl-s">"truthfulness"</span>: 
                    <span class="pl-kos">{</span>
                        <span class="pl-s">"Type"</span>: <span class="pl-kos">[</span>
                            <span class="pl-s">"1"</span><span class="pl-kos">,</span>
                            <span class="pl-s">"2"</span>
                        <span class="pl-kos">]</span><span class="pl-kos">,</span>
                        <span class="pl-s">"Rationale"</span>: <span class="pl-s">"The text suggests whitewater rafting on the Nile and diving in the Red Sea, which are not available in Cairo. These activities are factually incorrect and not aligned with the instruction."</span><span class="pl-kos">,</span>
                        <span class="pl-s">"Rating"</span>: <span class="pl-s">"3"</span><span class="pl-kos">,</span>
                        <span class="pl-s">"Rationale For Rating"</span>: <span class="pl-s">"The text provides some truthful information, but the inclusion of whitewater rafting and diving in the Red Sea causes partial hallucination and misunderstanding."</span>
                    <span class="pl-kos">}</span><span class="pl-kos">,</span>
                <span class="pl-s">"helpfulness"</span>: 
                    <span class="pl-kos">{</span>
                        <span class="pl-s">"Type"</span>: <span class="pl-kos">[</span>
                            <span class="pl-s">"1"</span><span class="pl-kos">,</span>
                            <span class="pl-s">"2"</span>
                        <span class="pl-kos">]</span><span class="pl-kos">,</span>
                        <span class="pl-s">"Rationale"</span>: <span class="pl-s">"The response is clear and relevant, providing useful information, but it does not address the limited walking ability of the grandparents or the adventure sports preferences of the sister, mom, and the user."</span><span class="pl-kos">,</span>
                        <span class="pl-s">"Rating"</span>: <span class="pl-s">"3"</span><span class="pl-kos">,</span>
                        <span class="pl-s">"Rationale For Rating"</span>: <span class="pl-s">"The text is correct and provides useful information, but it does not fully address the specific needs and preferences of the family."</span>
                    <span class="pl-kos">}</span>
            <span class="pl-kos">}</span>
        <span class="pl-kos">}</span><span class="pl-kos">,</span>
        <span class="pl-kos">{</span> <span class="pl-c">// completion 2</span>
            ...
        <span class="pl-kos">}</span><span class="pl-kos">,</span>
        <span class="pl-kos">{</span> <span class="pl-c">// completion 3</span>
            ...
        <span class="pl-kos">}</span><span class="pl-kos">,</span>
        <span class="pl-kos">{</span> <span class="pl-c">// completion 4</span>
            ...
        <span class="pl-kos">}</span>
    <span class="pl-kos">]</span>
<span class="pl-kos">}</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="{
    &quot;source&quot;: &quot;sharegpt&quot;, // the dataset where the instruction comes from
    &quot;instruction&quot;: &quot;I am going to cairo in June of this year, thinking of 4-5 days, what are the best things I could do? Going with family of 5, granddad, grandmom, sister, mom and me. No days can have too much walking but sister, me and mom like adventure sports&quot;,
    &quot;models&quot;: [ // four models that are randomly sampled from the model pool to complete the instruction
        &quot;falcon-40b-instruct&quot;,
        &quot;gpt-4&quot;,
        &quot;starchat&quot;,
        &quot;wizardlm-7b&quot;
    ],
    &quot;correct_answers&quot;: [&quot;None&quot;] // reference correct answers, set to None except TruthfulQA and FLAN
    &quot;incorrect_answers&quot;: [&quot;None&quot;] // reference incorrect answers, set to None except TruthfulQA and FLAN
    &quot;completions&quot;: [ // four completions corresponding to the four models
        { // completion 1
            &quot;model&quot;: &quot;falcon-40b-instruct&quot;,
            &quot;principle&quot;: &quot;helpfulness&quot;, // priciple that used to align model behavior
            &quot;custom_system_prompt&quot;: &quot;As an AI assistant, ensure your response offers the perfect blend of accuracy, positivity, and intrigue. Strive to be educational while keeping the user engaged.&quot;, // the system prompt corresponding to the principle, sampled from a pool of GPT-4 generated system prompts
            &quot;response&quot;: &quot;Cairo is a city that has something for everyone. Some of the best things you can do are visiting the Pyramids of Giza and the museums housing the ancient Egyptian artifacts, taking a camel ride in the desert, visiting the medieval neighborhoods such as Islamic Cairo or Khan El-Khalili, trying local food in traditional restaurants like Ful medames, or venturing into one of the city's many souks to shop for authentic goods. For adventure sports, you can go whitewater rafting on the Nile, hot air ballooning over the desert, or diving in the Red Sea. Just be mindful of the scorching heat and ensure everyone wears appropriate clothing and drinks enough water. Above all, have fun and enjoy your trip!&quot;,
            &quot;annotations&quot;: {
                &quot;instruction_following&quot;: 
                    {
                        &quot;Rating&quot;: &quot;2&quot;,
                        &quot;Rationale&quot;: &quot;The text only partially addresses the task goal by providing a general list of activities in Cairo without considering the specific needs of the family members, such as limited walking ability and adventure preferences.&quot;
                    },
                &quot;honesty&quot;: 
                    {
                        &quot;Rating&quot;: &quot;3&quot;,
                        &quot;Rationale&quot;: &quot;The response is confident but contains subtle mistakes. It suggests whitewater rafting on the Nile and diving in the Red Sea, which are not feasible for a 4-5 day trip to Cairo. The rest of the suggestions are accurate and relevant to the family's preferences.&quot;
                    },
                &quot;truthfulness&quot;: 
                    {
                        &quot;Type&quot;: [
                            &quot;1&quot;,
                            &quot;2&quot;
                        ],
                        &quot;Rationale&quot;: &quot;The text suggests whitewater rafting on the Nile and diving in the Red Sea, which are not available in Cairo. These activities are factually incorrect and not aligned with the instruction.&quot;,
                        &quot;Rating&quot;: &quot;3&quot;,
                        &quot;Rationale For Rating&quot;: &quot;The text provides some truthful information, but the inclusion of whitewater rafting and diving in the Red Sea causes partial hallucination and misunderstanding.&quot;
                    },
                &quot;helpfulness&quot;: 
                    {
                        &quot;Type&quot;: [
                            &quot;1&quot;,
                            &quot;2&quot;
                        ],
                        &quot;Rationale&quot;: &quot;The response is clear and relevant, providing useful information, but it does not address the limited walking ability of the grandparents or the adventure sports preferences of the sister, mom, and the user.&quot;,
                        &quot;Rating&quot;: &quot;3&quot;,
                        &quot;Rationale For Rating&quot;: &quot;The text is correct and provides useful information, but it does not fully address the specific needs and preferences of the family.&quot;
                    }
            }
        },
        { // completion 2
            ...
        },
        { // completion 3
            ...
        },
        { // completion 4
            ...
        }
    ]
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据集示例</font></font></h1><a id="user-content-dataset-example" class="anchor" aria-label="永久链接：数据集示例" href="#dataset-example"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这里我们展示一个 UltraFeedback 的例子</font></font></p>
<blockquote>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网友：</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我今年6月要去开罗，想想4-5天，我能做的最好的事情是什么？和一家五口一起去，爷爷，奶奶，姐姐，妈妈和我。没有一天可以走太多路，但姐姐、我和妈妈都喜欢冒险运动</font></font></p>
</blockquote>
<blockquote>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">助理 1（falcon-40b-instruct）：</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开罗是一座适合每个人的城市。您可以做的一些最好的事情是参观吉萨金字塔和收藏古埃及文物的博物馆，在沙漠中骑骆驼，参观伊斯兰开罗或汗哈利利等中世纪社区，尝试传统的当地美食前往 Ful medames 等餐厅，或者前往市内众多露天市场之一购买正宗商品。对于冒险运动，您可以在尼罗河上漂流、在沙漠上空乘坐热气球或在红海潜水。请注意炎热的天气，并确保每个人都穿着合适的衣服并喝足够的水。最重要的是，玩得开心并享受您的旅行！</font></font></p>
</blockquote>
<blockquote>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">助理2（gpt-4）：</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在开罗停留4-5天，考虑到祖父母步行能力有限，以下活动可满足您家人的喜好：\n\n1.参观吉萨金字塔和狮身人面像：这是开罗的必游景点，对于所有年龄段的人来说都是一次令人难以置信的体验。您可以租用骆驼、马或马车探索这里，既能满足祖父母的需求，又能保持愉快的体验。\n\n2.埃及博物馆： 这座位于解放广场的博物馆藏有超过 120,000 件埃及历史文物。可以为祖父母安排轮椅，让他们按照自己的节奏舒适地探索博物馆。\n\n3. Khan el-Khalili Bazaar：开罗著名的市场，适合购买纪念品和体验当地文化。游览期间有很多咖啡馆和餐馆可供您的家人休息。\n\n4.尼罗河游船：与家人在著名的尼罗河上享受晚餐游船。它包括娱乐活动和美丽的城市夜景。船上通常设有坡道和无障碍设施，供行动不便的乘客使用。\n\n5. Felucca Ride（三桅小帆船之旅）：白天，您可以体验一种更传统的帆船之旅，称为 Felucca（三桅小帆船）。这可以在休息日进行，也可以作为忙碌一天后的放松活动。\n\n对于冒险活动，您和您的妈妈和姐妹可以探索以下选项：\n\n1。滑沙：前往沙漠，在开罗郊外的沙丘上体验惊险刺激的滑沙体验。\n\n2.乘坐热气球：在卢克索乘坐热气球从高空体验埃及。虽然需要从开罗出发一日游，但这是一次令人难以置信的体验。\n\n3.四轮摩托车：在导游的带领下在沙漠中进行四轮摩托车冒险，欣赏标志性金字塔的全景。\n\n根据家人的喜好安排活动时间，并确保下午有休息时间。由于您担心祖父母的行走能力，请考虑聘请私人导游或参加轮椅无障碍旅游。</font></font></p>
</blockquote>
<blockquote>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">助理 3 (starchat)：</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以下是一家 5 口人&ZeroWidthSpace;&ZeroWidthSpace;在埃及开罗最值得做的事情：\n\n- 吉萨金字塔：吉萨金字塔位于开罗郊外，是任何人都必须参观的地方参观这座城市。胡夫大金字塔、哈夫拉大金字塔和门卡拉大金字塔构成了古代世界七大奇迹之一，令人印象深刻。\n\n- 埃及博物馆：埃及博物馆位于位于开罗市中心，拥有世界上最大、最令人印象深刻的古埃及艺术和文物收藏之一。游客可以探索博物馆丰富的藏品，其中包括木乃伊、石棺、雕像、绘画等。\n\n- 萨拉丁城堡：萨拉丁城堡位于俯瞰开罗的山上。享有城市的壮丽景色，是了解开罗丰富历史的绝佳场所。游客可以探索城堡内的各种博物馆和清真寺，还可以在美丽的花园中漫步。\n\n- Khan El-Khalili 集市：Khan El-Khalili 集市是开罗最受欢迎的景点之一，是一个熙熙攘攘的市场，游客可以找到从香料、陶器到衣服和珠宝的一切。这是购物和探索当地文化的好地方。\n\n- 亚历山大：亚历山大距离开罗仅一小时车程，是一座历史悠久的城市，拥有丰富的遗产。游客可以探索古罗马圆形剧场、Qaitbay 城堡和 Kom el-Shoqafa 地下墓穴。他们还可以乘船前往蒙塔扎岛，在那里可以看到著名的涂鸦墙和其他街头艺术。\n\n所有这些景点都适合家庭游玩，有很多值得一看和可以做的事情。尤其是吉萨金字塔，交通便利且维护良好，适合所有年龄段的游客。</font></font></p>
</blockquote>
<blockquote>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">助理 4 (wizardlm-7b)：</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您正在寻找冒险运动，您可以探索吉萨金字塔并乘坐热气球飞越沙漠。另一种选择是在沙漠中骑骆驼或在尼罗河中划皮划艇。对于适合家庭的活动，您可以参观埃及博物馆、开罗塔和汗哈利利集市。此外，您还可以前往历史名城亚历山大一日游，参观庞培之柱和罗马露天剧场。另一种选择是在导游的带领下游览约旦古城佩特拉，距离开罗只有很短的飞行时间。</font></font></p>
</blockquote>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">去做</font></font></h2><a id="user-content-to-do" class="anchor" aria-label="永久链接：待办事项" href="#to-do"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul class="contains-task-list">
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox" checked=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 UltraFeedback 训练奖励模型和批评模型（即将推出！）。</font></font></li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过 RLHF 增强开源法学硕士。</font></font></li>
<li class="task-list-item"><input type="checkbox" id="" disabled="" class="task-list-item-checkbox"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 UltraFeedback 扩展到多轮对话。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">局限性</font></font></h2><a id="user-content-limitations" class="anchor" aria-label="永久链接：限制" href="#limitations"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">尽管 GPT-4 可以为大多数样本提供对齐良好的注释和文本反馈，但我们必须注意到 GPT-4 也会犯错误。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">引文</font></font></h2><a id="user-content-citation" class="anchor" aria-label="永久链接：引文" href="#citation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="highlight highlight-text-bibtex notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">@misc</span>{<span class="pl-en">cui2023ultrafeedback</span>,
      <span class="pl-s">title</span>=<span class="pl-s"><span class="pl-pds">{</span>UltraFeedback: Boosting Language Models with High-quality Feedback<span class="pl-pds">}</span></span>, 
      <span class="pl-s">author</span>=<span class="pl-s"><span class="pl-pds">{</span>Ganqu Cui and Lifan Yuan and Ning Ding and Guanming Yao and Wei Zhu and Yuan Ni and Guotong Xie and Zhiyuan Liu and Maosong Sun<span class="pl-pds">}</span></span>,
      <span class="pl-s">year</span>=<span class="pl-s"><span class="pl-pds">{</span>2023<span class="pl-pds">}</span></span>,
      <span class="pl-s">eprint</span>=<span class="pl-s"><span class="pl-pds">{</span>2310.01377<span class="pl-pds">}</span></span>,
      <span class="pl-s">archivePrefix</span>=<span class="pl-s"><span class="pl-pds">{</span>arXiv<span class="pl-pds">}</span></span>,
      <span class="pl-s">primaryClass</span>=<span class="pl-s"><span class="pl-pds">{</span>cs.CL<span class="pl-pds">}</span></span>
}</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="@misc{cui2023ultrafeedback,
      title={UltraFeedback: Boosting Language Models with High-quality Feedback}, 
      author={Ganqu Cui and Lifan Yuan and Ning Ding and Guanming Yao and Wei Zhu and Yuan Ni and Guotong Xie and Zhiyuan Liu and Maosong Sun},
      year={2023},
      eprint={2310.01377},
      archivePrefix={arXiv},
      primaryClass={cs.CL}
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</article></div>
